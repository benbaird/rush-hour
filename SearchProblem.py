from Queue import Queue

class SearchProblem:
  """
  This class represents the superclass for a search problem.

  Programmers should subclass this superclass filling in specific
  versions of the methods that are stubbed below.
  """
  stop = False;	# class variable to end search - single variable accessible to
                # all instances of the class
  
  num_states = 0
  nodes_per_depth = []

  visited = [];	# class variable that holds the states visited along the
		# path to the current node - used to avoid loops
  nodesQ = Queue() # holds a list of nodes to be checked
  
  def __init__( self, state=None ):
    """
    Stub
    Constructor function for a search problem.

    Each subclass should supply a constructor method that can operate with
    no arguments other than the implicit "self" argument to create the
    start state of a problem.

    It should also supply a constructor method that accepts a "state"
    argument containing a string that represents an arbitrary state in
    the given search problem.

    It should also initialize the "path" member variable to a (blank) string.
    """
    raise NotImplementedError("__init__");

  def edges( self ):
    """
    Stub
    This method must supply a list or iterator for the Edges leading out 
    of the current state.
    """
    raise NotImplementedError("edges");

  def is_target( self ):
    """
    Stub
    This method must return True if the current state is a goal state and
    False otherwise.
    """

    raise NotImplementedError("is_target");

  def __repr__( self ):
    """
    This method must return a string representation of the current state
    which can be "eval"ed to generate an instance of the current state.
    """

    return self.__class__.__name__ + "( " + repr(self.state) + ")";

  def target_found( self ):
    """
    This method is called when the target is found.

    By default it prints out the path that was followed to get to the 
    current state.
    """
    print( "Solution: " + self.path );

  def continue_search( self ):
    """
    This method should return True if the search algorithm is to continue
    to search for more solutions after it has found one, or False if it
    should not.
    """
    return False;

  """
  Breadth first search, searches from each depth iteratively
  """
  def bfs( self ):
    node_depth = 0
    
    queue = Queue()
    queue.put((self,node_depth))

    # Keep searching if still nodes in q or depth not reached
    while ( (not SearchProblem.stop)
             and queue.qsize() > 0):

      SearchProblem.num_states += 1

      # Get the next node and it's depth from the queue    
      next = queue.get()
      node = next[0]
      node_depth = next[1]
      # Increment nodes at each depth
      try:
        SearchProblem.nodes_per_depth[node_depth] += 1
      except:
        SearchProblem.nodes_per_depth.append(1)   

      # Check if the current node is the destination
      if ( node.is_target() ):
        node.target_found()
        if node.continue_search(): 
          SearchProblem.stop = False;
        else:
          SearchProblem.stop = True; 
          return;

      # Check if current state is in the visited list yet
      if (node.state not in SearchProblem.visited):
        SearchProblem.visited.append( node.state );
      else:
        continue;

      # Get the edges for said node and add to the Queue
      for action in node.edges():
        # Check if we already visited this node
        if ( action.destination.state in SearchProblem.visited):
          continue;

        action.destination.path = node.path + str(action.label);

  #       Put edges at the end of the Queue
        queue.put( (action.destination, node_depth+1) )

class Edge:
  """
  This class represents an edge between two nodes in a SearchProblem.
  Each edge has a "source" (which is a subclass of SearchProblem), a
  "destination" (also a subclass of SearchProblem) and a text "label".
  """

  def __init__( self, source, label, destination ):
    """
    Constructor function assigns member variables "source", "label" and
    "destination" as specified.
    """
    self.source = source;
    self.label = label;
    self.destination = destination;

  def __repr__( self ):
    return "Edge(" + repr( self.source ) + "," + \
                     repr( self.label ) + "," + \
                     repr( self.destination ) + ")";

# README #

Python 2.7.2 program 

To clone repo:
  git@bitbucket.org:benbaird/rush-hour.git

To use program:
  python rush_hour.py < input.txt

### What is this repository for? ###

Solves the rush hour board game available at http://www.thinkfun.com/play-online/rush-hour/

Utilizes breadth first search to find a solution to the board in input.txt

0.3

### How do I get set up? ###

install python 2.7.2

###How to run tests ###
Create a file where each line has:
             A%%$

where A is a letter from A - K, X and denotes a car (2 spaces)

where A is a letter from O - R and denotes a truck (3 spaces)

where %% is a number from 0 - 5 and should be the top-left-most space of the vehicle

where $ is the orientation of the vehicle and can be H or V for horizontal and vertical respectively
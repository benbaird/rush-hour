from SearchProblem import *
import sys
from copy import copy,deepcopy
from Queue import *
from random import randrange


"""
    Allows solving of the rush hour game board through
    best first search and depth first search.

    Best first search allows the use of the following heuristics
            - Number of cars in the path of the red car
            - Number of blocks it takes to move a vehicle
              from out of the path of the red car

    TODO      
        - Generate Random board
"""
class RushHour(SearchProblem):
    cars = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J","K", "X"]
    trucks = ["O", "P", "Q", "R"]
    num_solutions = 0;
    def __init__( self, state, root= False ):
        self.state = state
        self.maxCarLen = 3
        self.path = ""
        if root:
            self.visited = []

    def test(self):
        self.edges()
        if ( self.is_target() ):
            self.target_found()

    """
    Checks pieces to see if they can move
        params: x,y locations of the piece to check
        return > 0 : moves up or right possible
        return 0 : no moves available
        return < 0 : moves down or left possible
    """
    def canMove ( self, board, x, y ):
        orientation = self.carOrientation(board, x, y);
        length = self.carLen(board, x, y);

        # Checks the tip of the car for blanks
        if (orientation == "H"):
            # Right
            if ( ((y+length) <= 5) and
                (board[x][y+length] == " ") ):
                return 1
            # Left
            if ( ((y-1) >= 0) and
                (board[x][y-1] == " ") ):
                return -1

        elif (orientation == "V"):
            # Down
            if ( ((x+length) <= 5) and
                (board[x+length][y] == " ") ):
                return -1
            # Up
            if ( ((x-1) >= 0 ) and 
                (board[x-1][y] == " ") ):
                return 1
        return 0;
    

    """ 
    Checks for the orientation of the piece
        params: x,y are the top-left-most 
                        square coordinates of a car        
        returns "H" for horzontal
        returns "V" for vertical
    """
    def carOrientation ( self, board, x, y ):
        # Check below if not on a wall
        if ( x < 5 ):
            if ( board[x+1][y] == board[x][y] ):
                return "V";
        # Check above if not on a wall
        if ( x > 0 ):
            if ( board[x-1][y] == board[x][y] ):
                return "V";

        # Check to the right
        if ( y < 5):
            if ( board[x][y+1] == board[x][y] ):
                return "H";
                
        # Check to the left           
        if ( y > 0):
            if ( board[x][y-1] == board[x][y] ):
                return "H";

    """
    Checks how long a peice is
        returns the length of the car
    """
    def carLen ( self, board, x, y ):
        length = 1
    
        orientation = self.carOrientation(board, x,y)
        if orientation == "H":
            for i in range(1,self.maxCarLen):
                if ( (y + i) < 6):
                    if board[x][y] == board[x][y+i]:
                        length += 1
                        
        elif orientation == "V":
            for i in range(1,self.maxCarLen):
                if ( (x + i) < 6) :
                    if board[x][y] == board[x+i][y]:
                        length += 1    
        return length

    """
    Moves a car right until it is blocked
        params: x,y are the top-left-most square coordinates of a
                    car
        return: the label of the piece moved
    """
    def moveRight(self, board, x, y):
        piece = board[x][y]
        offset = 0

        # See how far right can move the piece
        for i in range (1,6):
            if ( (i + y) > 5 ):
                break
            if ( board[x][y+i] != " " and
                 board[x][y+i] != piece ):
                break;
            offset += 1

        # Actually move the piece
        pieceLen = self.carLen(board, x,y)
        for p in range(0, pieceLen):
            currentPos = y + pieceLen - p - 1
            board[x][currentPos + (offset- (pieceLen-1))] = piece
            if ( (currentPos + (offset- (pieceLen-1))) !=
               currentPos ):
                board[x][currentPos] = " "
                
        return piece

    """
    Moves a car left until it is blocked
        params: x,y are the top-left-most square coordinates of a
                    car
        return: the label of the piece moved                    
    """
    def moveLeft(self, board, x, y):
        piece = board[x][y]
        offset = 0

        # See how far right can move the piece
        for i in range (1,6):
            if ( ( y - i) < 0 ):
                break
            if ( board[x][y-i] != " " and
                 board[x][y-i] != piece ):
                break;
            offset -= 1

        # Actually move the piece
        pieceLen = self.carLen(board, x,y)
        for p in range(y, (y+pieceLen)):
            board[x][p + offset] = piece
            board[x][p] = " "

        return piece

    """
    Moves a car up until it is blocked
        params: x,y are the top-left-most square coordinates of a
                    car
        return: the label of the piece moved                    
    """
    def moveUp(self, board, x, y):
        piece = board[x][y]
        offset = 0

        # See how far right can move the piece
        for i in range (1,6):
            if ( ( x - i) < 0 ):
                break
            if ( board[x - i][y] != " " and
                 board[x - i][y] != piece ):
                break;
            offset -= 1

        # Actually move the piece
        pieceLen = self.carLen(board, x,y) 
        for p in range(x, (x+pieceLen)):
            board[p + offset][y] = piece
            board[p][y] = " "
        return piece
        
    """
    Moves a car downuntil it is blocked
        params: x,y are the top-left-most square coordinates of a
                car
        return: the label of the piece moved                
    """
    def moveDown(self, board, x, y):
        piece = board[x][y]
        offset = 0

        # See how far right can move the piece
        for i in range (1,6):
            if ( ( x + i) > 5 ):
                break
            if ( board[x + i][y] != " " and
                 board[x + i][y] != piece ):
                break;
            offset += 1

        # Actually move the piece
        pieceLen = self.carLen(board, x, y)
#        print pieceLen
        for p in range(0, pieceLen):
            currentPos = x + pieceLen - p - 1
            # Go to the car's end and move it individually
            board[currentPos + (offset- (pieceLen-1))][y] = piece

            # Overwrites the previous position if not used by new position
            if ( (currentPos + (offset- (pieceLen-1)) ) !=
               currentPos ):
                board[currentPos][y] = " "

        return piece        
    """
    Visual representation of the board is printed out
    """
    def printBoard( self, board ):
        for row in range (0,6):
            print board[row]

    """ 
    Checks the number of blocks blocking the red car
    from exiting
        return 0  : the path is clear
        return >0 : the number of blocks taken up
    """
    def numInPath ( self, board, piece, direction, x, y ):
        numBlocks = 0
        redCount = 0

        # Check row/column based on orientation
        if direction == "R":
            # Movement right
            for i in range(0,(6-y)):
                if (y + i) > 5:
                    break;
                if (board[x][y+i] != piece and board[x][y+i] != " "):
                    numBlocks += 1

        elif direction == "L":
            # Movement left
            for i in range(0,6): # impossible to have x,y starting in last column
                if (y - i) < 0:
                    break;
                if (board[x][y-i] != piece and board[x][y-i] != " "):
                    numBlocks += 1        

        elif direction == "D":
            # Movement down
            for i in range(0,(6-x)):
                if (x + i) > 5:
                    break;
                if (board[x+i][y] != piece and board[x+i][y] != " "):
                    numBlocks += 1

        elif direction =="U":
            # Movement up
            for i in range(0,6):
                if (x - i) < 0:
                    break;
                if (board[x - i][y] != piece and board[x-i][y] != " "):
                    numBlocks += 1    
        return numBlocks;

    """ 
    Checks if the board is at the exit
        return true : car is at the exit
        return false : car is not at the exit 
    """
    def is_target ( self ):
        return self.state[2][4] == "X" and self.state[2][5] == "X"

    """ 
    When a solution is found it prints it out
    """   
    def target_found ( self ):
        RushHour.num_solutions += 1
#        print "Solution: ", self.path
        print self.path # Returns just the solution moves

    """
    Finds all pieces in the board
    """
    def findPieces( self ):
        pieces = []
        markedBoard = [[0]*6 for x in range(6)]
    
        # Go through board and check for pieces
        for x in range(0,6):
            for y in range(0,6):
                if (markedBoard[x][y] != "*" and 
                    self.state[x][y] != " "):
                    length = self.carLen(self.state, x,y)
                    orientation = self.carOrientation(self.state, x,y)
                
                    pieces.append((x, y, orientation));
                    for i in range (0, length):    
                        if orientation == "H":
                            markedBoard[x][y+i] = "*"
                        elif orientation == "V":
                            markedBoard[x+i][y] = "*"
        return pieces
    """
    Provides all possible moves in the current game state
        return list of all possible moves
    """
    def edges ( self ):
        my_edges = []
        
        # Find each piece that can move and add it to the list
        pieces = self.findPieces()
        for p in pieces:
            cloneBoard = deepcopy(self.state); 
            move = self.canMove( self.state, p[0], p[1])
            if ( move > 0):

                # Can move up/right
                if (p[2] == "H"):
                    label = self.moveRight(cloneBoard, p[0], p[1]);
                    my_edges.append(Edge ( self, label + "R\n", RushHour (cloneBoard) ) )
                    
                if (p[2] == "V"):
                    label = self.moveUp(cloneBoard, p[0], p[1]);
                    my_edges.append( Edge ( self, label + "U\n", RushHour (cloneBoard) ) )

            elif (move < 0):
                # Can move down/left
                if (p[2] == "H"):
                    label = self.moveLeft(cloneBoard, p[0], p[1]);
                    my_edges.append( Edge ( self, label + "L\n", RushHour (cloneBoard) ) )
                if (p[2] == "V"):
                    label = self.moveDown(cloneBoard, p[0], p[1]);
                    my_edges.append( Edge ( self, label + "D\n", RushHour (cloneBoard) ) )
        return my_edges

    """
    Displays the number of states visited and nodes at each depth
    """
    def printStats( self ):
        print "Number of states visited: ", RushHour.num_states
        for x in range(len(RushHour.nodes_per_depth)):
            print  "Depth", x, ": ", RushHour.nodes_per_depth[x]


    """
    Finds the first difference in the two boards
        return x,y locations of the piece in the first board 
                    and of the second
            if no differences then returns the last square
    """
    def getPieceLocation( self, board, piece):
        for x in range (0,6):
            for y in range (0,6):
                if (board[x][y] == piece):
                    return (x, y)


    """
    Best First Search algorithm that takes allows for a choice between two
    heuristics to use
        param heuristic : "redCarPathSearch" or "clearTheWaySearch"
    """
    def bestFirstSearch( self, heuristic ):
        node_depth = 0
        queue = PriorityQueue()
        queue.put((0,self,node_depth))


        # Keep searching if still nodes in q or depth not reached
        while ( (not RushHour.stop)
               and queue.qsize() > 0):
               
            RushHour.num_states += 1
            
            # Get the next node and it's depth from the queue    
            next = queue.get()
            node = next[1]
            node_depth = next[2]
            
#            self.printBoard( node.state)
#            print node_depth, next[0]
            
            # Increment nodes at each depth
            try:
                RushHour.nodes_per_depth[node_depth] += 1
            except:
                RushHour.nodes_per_depth.append(1)   

            # Check if the current node is the destination
            if ( node.is_target() ):
                node.target_found()
                if node.continue_search(): 
                    RushHour.stop = False;
                else:
                    RushHour.stop = True; 
                    return;

            # Check if current state is in the visited list yet
            if (node.state not in RushHour.visited):
                RushHour.visited.append( node.state );
            else:
                continue;

            # Get the edges for said node and add to the Queue
            for action in node.edges():
              # Check if we already visited this node
                if ( action.destination.state in RushHour.visited):
                    continue;

                action.destination.path = node.path + str(action.label);
                
                # Choose which heuristic to use
                if (heuristic == "redCarPathSearch"):
                    heuristic_value = self.redCarPathSearch( action.destination.state,
                                                             action.label[1])
                        
                elif (heuristic == "clearTheWaySearch"):
                    heuristic_value = self.clearTheWaySearch( action.destination.state,
                                                             action.label[0],
                                                             action.label[1]);

                # Red car has priority to finish the game
                if (action.label[0] == "X" and heuristic_value == 0):
                    heuristic_value = 0
                else:
                    # Priority to pieces that can move freely in a direction
                    heuristic_value += node_depth
                    if heuristic_value == 0:
                        heuristic_value -= 1
                    
                    if self.canMoveFromRedPath( action.destination.state ):
                        heuristic_value -= 1
    #                    self.printBoard ( action.destination.state )
    #                    print heuristic_value, action.label[0], "\n"

                # Put edges at the end of the Queue
                queue.put( (heuristic_value, action.destination, node_depth+1) )

    """
    Checks whether a piece can be moved from the red car's path
    """
    def canMoveFromRedPath ( self, board ):
        cloneBoard = deepcopy(board)
        redPos = self.getPieceLocation( cloneBoard, "X")

        # Car car exit next turn
#        if redPos[1] +2 > 5:
#            return True

        # Look for pieces in the path
        for y in range( ( redPos[1] + 2), 6 ):
            if ( cloneBoard[2][y] != " "):
                piecePos = self.getPieceLocation(cloneBoard, cloneBoard[2][y]);
                # Try to move piece out of the path
                if (self.canMove( cloneBoard, piecePos[0], piecePos[1]) ):
                    self.moveDown(cloneBoard, piecePos[0], piecePos[1]);
                    
                    # Piece is out of the way, yay
                    if (cloneBoard[2][y] == " " ):
                        return True
                    
                    else:
                        piecePos = self.getPieceLocation(cloneBoard, cloneBoard[2][y])
                        self.moveUp( cloneBoard, piecePos[0], piecePos[1] );
                        if (cloneBoard[2][y] == " "):
                            return True
        return False
    

    """
    Prioritizes moves that have less vehicles in it's path
    h(x) = number of moves in the path of a vehicle
    f(x) = h(x) + moveNumber
    """
    def clearTheWaySearch ( self, board, piece, direction):
        # Find out coordinates of the piece that was moved
        # Calculate the closeness to a solution of each move
        coords = self.getPieceLocation ( board, piece )
        heuristic_value = self.numInPath(board, piece, direction, coords[0], coords[1])

        return heuristic_value
                
    """
    Alternative heuristic for a best first search
    Based on how many blocks are infront of the red car. With the assumption
    that less blocks infront of the red car is better
    h(x) = blocks in front of red car
    f(x) = h(x) + turn number
    """
    def redCarPathSearch ( self, board, direction):
        coords = self.getPieceLocation(board, "X");
        heuristic_value = self.numInPath(board, "X", direction, coords[0], coords[1])

        return heuristic_value

    """
    Returns the difficulty of the board solved
    Based on:
             - how many pieces are oriented the same way 
             - the number of pieces
             - the number of moveable pieces
    """
    def getDifficulty ( self ):
        orientation = 0;
        moveable = 0;
        pieces = self.findPieces()
        numPieces = len(pieces)
        
        for p in pieces:
            if self.carOrientation( self.state, p[0], p[1]) == "H":
                orientation +=1
            else:
                orientation -=1
            moveable += abs(self.canMove(self.state, p[0], p[1]))


        print orientation, numPieces, moveable, self.redCarPathSearch(board, "R"), self.canMoveFromRedPath(self.state)
        if ( (orientation in range (-2,2)) and (numPieces >= 10) and (moveable <= 5) and 
            (self.redCarPathSearch(board, "R") >= 1) and (self.canMoveFromRedPath(self.state) == False) ): 
            return "Hard"

        if ( (orientation in range (-2,2)) and (numPieces >= 8) and (moveable < 10) and 
            (self.redCarPathSearch(board, "R") >= 1) and (self.canMoveFromRedPath(self.state) == False)):
            return "Medium"

        else:
            return "Easy"


def genBoard ():
    board = [[" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "], #Exit 
             [" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "],]
    col = randrange(0,4)
    
    # Random red car placement
    board[2][col] = "X"
    if (col > 0):
        upDo = [-1, 1]
        print upDo[0]
        print col - upDo[randrange(0,1)]
        board[2][col - upDo[randrange(0,1)]] = "X"
    else:
        board[2][col+1] = "X"

    # Randomly select vehicles to use
    vehicles = []
    
    for truck in RushHour.trucks:
        vehicles.append(truck)
    for car in RushHour.cars:
        if car == "X":
            continue;
        vehicles.append(car)
    orientations = ["H", "V"]
    
    game = RushHour(board)
    
    collisions = 0
    
    while (collisions < 10) and len(vehicles) > 0:
        piece = vehicles[randrange(0,len(vehicles))]
        orientation = orientations[randrange(0,2)]
        x = randrange(0,6)
        y = randrange(0,6)

        # Makes sure there is a space in the row or column
        for checkX in range (0, 6):
            if ( ( " " not in board[x]) and
                ( (" " not in board[checkX][y])) ):
                continue;

        # Make sure there are no horizontal pieces in the red car's row
        if ( x == 2 and orientation == "H"):
            continue;    
                
        # Try to insert the piece
        if (insertPiece(board, piece, x, y, orientation) == 0):
            # Inserted piece successfully!
            vehicles.remove(piece)
            game.printBoard(board)
            print "~~~~"
        else:
#            board[x][y] = " "
            collisions += 1
    return board

"""
Inserts piece into the board
    X     : Red car
    A - K : are cars

    O - R : are trucks
"""
def insertPiece(board, piece, x, y, orientation):
    if ( not( (piece in RushHour.cars) or (piece in RushHour.trucks) ) ):
#        print "Error: Invalid piece to insert into board"
        return -1
    
    # Insertion of piece
    if (board[x][y] != " "):
#        print "Error: Piece already in position", x,y
        return -1;
        
    if orientation == "H":
        if (y + 1) > 5:
#            print ("Error: Piece cannot be placed horzontally", piece, x, y)
            return -1
            # Insertion of piece
        if (board[x][y+1] != " "):
#            print "Error: Piece already in position", x,y
            return -1;
        board[x][y] = piece
        board[x][y+1] = piece
        
        if piece in RushHour.trucks:
            if (y + 2) > 5:
#                print ("Error: Truck cannot be placed here", piece, x, y)
                board[x][y+1] = " "
                board[x][y] = " "
                return -1
            # Insertion of piece
            if (board[x][y+2] != " "):
#                print "Error: Piece already in position", x,y
                board[x][y+1] = " "
                board[x][y] = " "
                return -1;
                
            board[x][y+2] = piece
    elif orientation == "V":
        if (x + 1) > 5:
#            print ("Error: Piece cannot be placed vertically here", piece, x,y)
            board[x][y] = " "
            return -1            

        if (board[x+1][y] != " "):
#            print "Error: Piece already in position", x,y
            board[x][y] = " "
            return -1;

        board[x][y] = piece        
        board[x+1][y] = piece
        if piece in RushHour.trucks:
            if (x + 2) > 5:
#                print ("Error: Truck cannot be placed vertically", piece, x,y)
                board[x+1][y] = " "
                board[x][y] = " "
                return -1                

            if (board[x+2][y] != " "):
#                print "Error: Piece already in position", x,y
                board[x+1][y] = " "
                board[x][y] = " "
                return -1;

            board[x+2][y] = piece
    return 0
    
if __name__ == "__main__":
    board = [[" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "], #Exit 
             [" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "],
             [" ", " ", " ", " ", " ", " "],]

    # To read file from stdin: rush_hour.py < input.txt
    for line in sys.stdin:
         insertPiece(board, line[0], int(line[1]), int(line[2]), line[3])

#    board = genBoard()

    game = RushHour(board);
    print "Starting Search"
    game.printBoard(game.state);
    print game.getDifficulty()
        
    game.bestFirstSearch("clearTheWaySearch");
#    game.bestFirstSearch("redCarPathSearch");

    if game.num_solutions < 0:
        print "No Solutions found :("
    else:
        print "Number of solutions found: ", RushHour.num_solutions
    game.printStats();
